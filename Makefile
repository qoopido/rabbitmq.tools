install:
	go get ./...

update:
	go get -t -u ./...

build:
	mkdir -p ./bin
	env GOOS=linux GOARCH=arm go build -ldflags '-w -extldflags "-static"' -o ./bin/producer-queue.linux.arm ./producer-queue
	env GOOS=linux GOARCH=arm64 go build -ldflags '-w -extldflags "-static"' -o ./bin/producer-queue.linux.arm64 ./producer-queue
	env GOOS=linux GOARCH=amd64 go build -ldflags '-w -extldflags "-static"' -o ./bin/producer-queue.linux.amd64 ./producer-queue
	env GOOS=darwin GOARCH=amd64 go build -ldflags '-w -extldflags "-static"' -o ./bin/producer-queue.darwin.amd64 ./producer-queue
	env GOOS=linux GOARCH=arm go build -ldflags '-w -extldflags "-static"' -o ./bin/producer-exchange.linux.arm ./producer-exchange
	env GOOS=linux GOARCH=arm64 go build -ldflags '-w -extldflags "-static"' -o ./bin/producer-exchange.linux.arm64 ./producer-exchange
	env GOOS=linux GOARCH=amd64 go build -ldflags '-w -extldflags "-static"' -o ./bin/producer-exchange.linux.amd64 ./producer-exchange
	env GOOS=darwin GOARCH=amd64 go build -ldflags '-w -extldflags "-static"' -o ./bin/producer-exchange.darwin.amd64 ./producer-exchange
	env GOOS=linux GOARCH=arm go build -ldflags '-w -extldflags "-static"' -o ./bin/consumer.linux.arm ./consumer
	env GOOS=linux GOARCH=arm64 go build -ldflags '-w -extldflags "-static"' -o ./bin/consumer.linux.arm64 ./consumer
	env GOOS=linux GOARCH=amd64 go build -ldflags '-w -extldflags "-static"' -o ./bin/consumer.linux.amd64 ./consumer
	env GOOS=darwin GOARCH=amd64 go build -ldflags '-w -extldflags "-static"' -o ./bin/consumer.darwin.amd64 ./consumer
