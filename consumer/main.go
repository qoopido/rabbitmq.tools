package main

import (
	"errors"
	"flag"
	"log"
	"os/exec"
	"strings"
	"sync"

	"github.com/isayme/go-amqp-reconnect/rabbitmq"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func getCommand(command string, arguments []string) string {
	return strings.Join(append([]string{command}, arguments...), " ")
}

func main() {
	uri := flag.String("uri", "amqp://guest:guest@127.0.0.1:5672/", "URI")
	queue := flag.String("queue", "", "Queue")

	flag.Parse()

	args := flag.Args()

	if len(args) < 1 {
		failOnError(errors.New("No command specified"), "Missing argument")
	}

	command := args[0]
	arguments := args[1:]

	if *uri == "" {
		failOnError(errors.New("No uri specified"), "Missing argument")
	}

	if *queue == "" {
		failOnError(errors.New("No queue specified"), "Missing argument")
	}

	log.Printf("Starting up...")

	conn, err := rabbitmq.Dial(*uri)
	failOnError(err, "Failed to connect")
	defer conn.Close()

	log.Printf("Connection established")

	ch, err := conn.Channel()
	failOnError(err, "Failed to open channel")
	defer ch.Close()

	log.Printf("Channel opened")

	err = ch.Qos(
		1,     // prefetch count
		0,     // prefetch size
		false, // global
	)
	failOnError(err, "Failed to set QoS")

	log.Printf("Channel initialized")

	msgs, err := ch.Consume(
		*queue, // queue
		"",     // consumer
		false,  // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	failOnError(err, "Failed to register consumer")

	log.Printf("Consumer registered")

	go func() {
		for msg := range msgs {
			message := string(msg.Body[:])
			args := append(arguments, message)

			log.Printf("Received message %s", message)

			cmd := exec.Command(command, args...)
			err := cmd.Run()
			if err == nil {
				msg.Ack(false)
				log.Printf("Successfully executed %s", getCommand(command, args))
			} else {
				msg.Nack(false, false)
				log.Printf("Failed to execute %s", getCommand(command, args))
				log.Printf("Error %s", err)
			}
		}
	}()

	log.Printf("Waiting for messages...")

	wg := sync.WaitGroup{}
	wg.Add(1)
	wg.Wait()
}
