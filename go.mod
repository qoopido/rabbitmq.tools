module gitlab.com/qoopido/rabbitmq.tools

go 1.15

require (
	github.com/isayme/go-amqp-reconnect v0.0.0-20180930040740-e71660afb5ca
	github.com/streadway/amqp v1.0.0
)
