package main

import (
	"errors"
	"flag"
	"log"
	"strings"

	"github.com/streadway/amqp"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func main() {
	uri := flag.String("uri", "amqp://guest:guest@127.0.0.1:5672/", "URI")
	exchange := flag.String("exchange", "", "Exchange")
	key := flag.String("key", "", "Routing key")

	flag.Parse()

	message := strings.Join(flag.Args(), " ")

	if message == "" {
		failOnError(errors.New("No message specified"), "Missing argument")
	}

	if *uri == "" {
		failOnError(errors.New("No uri specified"), "Missing argument")
	}

	if *exchange == "" {
		failOnError(errors.New("No exchange specified"), "Missing argument")
	}

	if *key == "" {
		failOnError(errors.New("No routing key specified"), "Missing argument")
	}

	conn, err := amqp.Dial(*uri)
	failOnError(err, "Failed to connect")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open channel")
	defer ch.Close()

	err = ch.Publish(
		*exchange, // exchange
		*key,      // routing key
		false,     // mandatory
		false,     // immediate
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "text/plain",
			Body:         []byte(message),
		})
	failOnError(err, "Failed to publish")
	log.Printf("Successfully published %s", message)
}
